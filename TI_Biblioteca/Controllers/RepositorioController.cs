﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TI_Biblioteca.Models;

namespace TI_Biblioteca.Controllers
{
    public class RepositorioController : Controller
    {
        // GET: Repositorio
        public ActionResult Index()
        {
            DataTable dtdados = Repositorio.ListarRepositorios();
            return View(dtdados);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddRepo(Repositorio repositorio)
        {
            Repositorio repo = repositorio;
            var retorno = repo.AdicionarRepositorio();

            if (retorno == string.Empty)
            {
                return RedirectToAction("Index", "Repositorio");
            } else
            {
                return Content("Erro: " + retorno);
            }    
        }

        public ActionResult Details(string ID)
        {
            if (ID == string.Empty)
            {
                return Content("Erro");
            } 
            var requestID = Convert.ToInt32(ID);

            Repositorio repo = new Repositorio();
            repo.Details(requestID);

            return View(repo);
        }
    }
}