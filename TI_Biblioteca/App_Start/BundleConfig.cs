﻿using System.Web;
using System.Web.Optimization;

namespace TI_Biblioteca
{
    public class BundleConfig
    {
        // Para obter mais informações sobre o agrupamento, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Static/js/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Static/js/jquery.validate*"));

            // Use a versão em desenvolvimento do Modernizr para desenvolver e aprender. Em seguida, quando estiver
            // pronto para a produção, utilize a ferramenta de build em https://modernizr.com para escolher somente os testes que precisa.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Static/js/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Static/js/bootstrap.js",
                      "~/Static/js/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Static/css/bootstrap.css",
                      "~/Static/css/site.css")
            );

            //bundles.Add(new StyleBundle("~/Content/MaterialThemeCSS").Include(
            //          "~/Static/css/bootstrap-material-design.min.css",
            //          "~/Static/css/ripples.min.css"
            //          )
            //);

            //bundles.Add(new ScriptBundle("~/bundles/MaterialThemeJS").Include(
            //          "~/Static/js/material.js",
            //          "~/Static/js/ripples.js"
            //          )
            //);
        }
    }
}
