﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace TI_Biblioteca.Models
{
    public class Repositorio
    {
        public Int32 Id { get; set; }
        public Char Ativo { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }               

        public static DataTable ListarRepositorios()
        {
            DataTable dtdados = null;

            try
            {
                //string sql = "select id, ativo, titulo, substr(descricao,1,100) || case when length(descricao) > 100 then '...' end descricao from tib_repositorio order by id desc";
                //dtdados = DBConnection.Select(sql);

                OleDbCommand c = new OleDbCommand();
                c.CommandText = "select id, ativo, titulo, substr(descricao,1,100) || case when length(descricao) > 100 then '...' end descricao from tib_repositorio order by id desc";

                //dtdados = DBConnection.Select(sql);
                dtdados = DBConnection.Select(c);
            }
            catch (Exception er)
            {
                Console.Out.WriteLine(er.Message);
            }

            return dtdados;
        }

        public String AdicionarRepositorio()
        {
            var resultado = "Erro desconhecido";

            try
            {
                OleDbCommand c = new OleDbCommand();
                c.CommandText = "insert into tib_repositorio(titulo, descricao) values (?, ?)";
                c.Parameters.AddWithValue("1", this.Titulo);
                c.Parameters.AddWithValue("2", this.Descricao);                

                resultado = DBConnection.Insert(c);
            }
            catch (Exception er)
            {
                Console.Out.WriteLine(er.Message);
                resultado = er.Message;
            }

            return resultado;
        }

        public Repositorio Details(int ID)
        {
            DataTable dtdados = null;

            try
            {
                OleDbCommand c = new OleDbCommand();
                c.CommandText = "select id, ativo, titulo, descricao from tib_repositorio where id = ?";
                c.Parameters.AddWithValue("1", ID);

                dtdados = DBConnection.Select(c);
                this.Id = Convert.ToInt32(dtdados.Rows[0]["Id"]);
                this.Ativo = Convert.ToChar(dtdados.Rows[0]["Ativo"]);
                this.Titulo = Convert.ToString(dtdados.Rows[0]["Titulo"]);
                this.Descricao = Convert.ToString(dtdados.Rows[0]["Descricao"]);
            }
            catch (Exception er)
            {
                Console.Out.WriteLine(er.Message);
                return null;
            }

            return this;
        }
    }
}