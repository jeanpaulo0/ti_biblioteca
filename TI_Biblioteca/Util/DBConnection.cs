﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace TI_Biblioteca
{
    public class DBConnection
    {

        private static string ConnString;
        private static OleDbConnection connection;

        private static string GetConnectionString()
        {
            if (ConnString != string.Empty)
            {
                try
                {
                    //ConnString = ConfigurationManager.ConnectionStrings["OracleTasyTESTE"].ConnectionString;
                    ConnString = ConfigurationManager.ConnectionStrings["OracleTasy"].ConnectionString;
                }
                catch (Exception er)
                {
                    return er.Message;
                }
            }
            else
            {
                return ConnString;
            }

            return ConnString;
        }

        private static void SetConnection()
        {
            connection = new OleDbConnection(GetConnectionString());
        }

        private static OleDbConnection GetConnection()
        {
            return connection;
        }

        private static void OpenConnection()
        {
            SetConnection();
            connection.Open();
        }

        private static void CloseConnection()
        {
            connection.Close();
        }

        public static DataTable Select(OleDbCommand sql)
        {
            DataTable resultado = null;

            try
            {
                //OpenConnection();
                //OleDbCommand comm = new OleDbCommand(sql, connection);
                //OleDbDataAdapter msda = new OleDbDataAdapter(comm);

                //resultado = new DataTable();
                //msda.Fill(resultado);

                OpenConnection();
                OleDbCommand comm = sql;
                comm.Connection = connection;
                OleDbDataAdapter msda = new OleDbDataAdapter(comm);

                resultado = new DataTable();
                msda.Fill(resultado);
            }
            catch (Exception er)
            {
                Console.Out.WriteLine(er.Message);
            }
            finally
            {
                CloseConnection();
            }

            return resultado;
        }

        public static string Insert(OleDbCommand sql)
        {
            var resultado = "Erro desconhecido";

            try
            {
                OpenConnection();
                //OleDbCommand comm = new OleDbCommand(sql, connection);
                //comm.ExecuteNonQuery();

                OleDbCommand comm = sql;
                comm.Connection = connection;
                comm.ExecuteNonQuery();

                resultado = string.Empty;
            }
            catch (Exception er)
            {
                Console.Out.WriteLine(er.Message);
                resultado = er.Message;
            }
            finally
            {
                CloseConnection();
            }

            return resultado;
        }

    }
}